<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Book::all();//to find all the books that exist
        //$id=Auth::id();
        //$books=User::find($id)->books;//to find the books that id 1 created
        return view('books.index',['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create Books..");
        } 
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create Books..");
        } 
        //$validated = $request->validated();
        $v = Validator::make($request->all(), [
            'title' => 'required|string|max:20',
            'author' => 'required|string|max:20',
        ]);
    
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        $books = new Book();
        $id=Auth::id();
        $books->title = $request->title;
        $books->author = $request->author;
        $books->user_id = $id;
        $books->save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create Books..");
        } 
        $books = Book::find($id);
        return view('books.edit',['books'=>$books]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$books = Book::find($id);
        //$books -> update($request->all());
        //return redirect('books');
        $books = Book::find($id);
        if (Gate::denies('manager')) {
            if($books->status==0){
                if(!$books->user->id == Auth::id()) return(redirect('books'));
                $books -> update($request->except(['_token']));
                if($request->ajax()){
                    return Response::json(array('result'=>'success', 'status'=>$request->status),200);
                }}  
                return redirect('books');
        } 
        if(!$books->user->id == Auth::id()) return(redirect('books'));
        $books -> update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create Books..");
        } 
        $books = Book::find($id);
        $books->delete();
        return redirect('books');

    }
}
