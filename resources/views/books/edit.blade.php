@extends('layouts.app')
@section('content')
<h1>Edit Book</h1>
<form method = 'post' action="{{action('BookController@update', $books->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Book to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$books->title}}">
</div>
<div class = "form-group">
    <label for = "author">Author to Update:</label>
    <input type= "text" class = "form-control" name= "author" value = "{{$books->author}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>
</form>
<form method = 'post' action="{{action('BookController@destroy', $books->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Book">
</div>

</form>
@endsection