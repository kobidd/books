<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'jack',
                        'email' => 'jack@gmail.com',
                        'password' =>Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'manager',
                ],
                [
                        'name' => 'john',
                        'email' => 'john@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'manager',
                ],
                [
                        'name' => 'ron',
                        'email' => 'ron@gmail.com',
                        'password' => Hash::make('12345678'),
                        'created_at' => date('Y-m-d G:i:s'),
                        'role'=>'manager',
                ],
                [
                    'name' => 'akiva',
                    'email' => 'a@a.com',
                    'password' => Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role'=>'reader',
            ],
                    ]);
    }
}
